record(bi, "$(PREFIX)$(SGNL)-WRN-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WRN-RB")

    field(DESC, "$(DESC) Warn")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=0" )
    field(ZNAM, "OK")
    field(ONAM, "WRN")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-Ilck-RB") {
    alias("$(PREFIX)$(SGNL)-INT-RB")
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-INT-RB")

    field(DESC, "$(DESC) Interlock")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=1" )
    field(ZNAM, "OK")
    field(ONAM, "ITLCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-DIS-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-DIS-RB")

    field(DESC, "$(DESC) Disabled")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=2" )
    field(FLNK, "$(PREFIX)$(SGNL)-DIS-INT")
    field(ZNAM, "ENABLED")
    field(ONAM, "DISABLED")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-FRC-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-FRC-RB")

    field(DESC, "$(DESC) Simulated")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=3" )
    field(FLNK, "$(PREFIX)$(SGNL)-FRC-INT")
    field(ZNAM, "N-SIMM")
    field(ONAM, "SIMM")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-NFRC-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-NFRC-RB")

    field(DESC, "$(DESC) Cant Simul")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=4" )
    field(ZNAM, "C-SIMM")
    field(ONAM, "N-SIMM")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-NLTCH-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-NLTCH-RB")

    field(DESC, "$(DESC) Latch Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=5" )
    field(ZNAM, "LTCHD")
    field(ONAM, "N-LTCHD")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-ISFRST-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ISFRST-RB")

    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=6" )
}

record(bi, "$(PREFIX)$(SGNL)-LOALR-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-LOALR-RB")

    field(DESC, "$(DESC) LOW Alarm")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=7" )

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL)-HIALR-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-HIALR-RB")

    field(DESC, "$(DESC) HIGH Alarm")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=0" )

    info(ARCHIVE_THIS, "")
}

record(waveform, "$(PREFIX)$(SGNL)-TMSTMP-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TMSTMP-RB")

    field(DESC, "$(DESC) Timestamp wf")
    field(SCAN, "I/O Intr")
    field(DTYP, "S7plc")
    field(INP, "@$(PLC)/$(SOFF)+2 T=TIME")
    field(FTVL, "CHAR")
    field(NELM, "8")
    field(FLNK, "$(PREFIX)$(SGNL)-TMSTMP-Conv")
}

record(aSub, "$(PREFIX)$(SGNL)-TMSTMP-Conv") {
    field(DESC, "$(DESC) Time Conv")
    field(SNAM, "convert_timestamp")
    field(INPA, "$(PREFIX)$(SGNL)-TMSTMP-RB NPP")
    field(FTA, "CHAR")
    field(FTVA, "STRING")
    field(NOA, "8")
    field(OUTA, "$(PREFIX)$(SGNL)-TMSTMP PP")
    field(BRSV, "INVALID")
}

record(stringin, "$(PREFIX)$(SGNL)-TMSTMP") {
    field(DESC, "$(DESC) Timestamp")
}

record(mbbi, "$(PREFIX)$(SGNL)-DTYP-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-DTYP-RB")

    field(DESC, "$(DESC) Signal type")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+10 T=BYTE" )
    field(ZRVL, "0")
    field(ZRST, "Unknow")
    field(ONVL, "1")
    field(ONST, "TSn")
    field(TWVL, "2")
    field(TWST, "PSn")
    field(THVL, "3")
    field(THST, "VSn_0_10v")
    field(FRVL, "27")
    field(FRST, "ISn_IP")
    field(SXVL, "5")
    field(SXST, "ISn_4_20mA")
    field(SVVL, "49")
    field(SVST, "FSn_05_150_l_min")
    field(EIVL, "50")
    field(EIST, "FSn_8_200_l_min")
    field(NIVL, "8")
    field(NIST, "PSn_0_10_bar")
    field(TEVL, "9")
    field(TEST, "TSn_-5_75_C")
    field(ELVL, "10")
    field(ELST, "TSn_PT100")
    field(TVVL, "11")
    field(TVST, "TSn_TC")
    field(TTVL, "12")
    field(TTST, "COLL_DISP")
    field(FTVL, "13")
    field(FTST, "BODY_DISP")
    field(SXVL, "53")
    field(SXST, "FSn_0_50_l_min")
    field(SXVL, "54")
    field(SXST, "FSn_0_250_l_min")
    field(SXVL, "55")
    field(SXST, "FSn_0_333_l_min")
    field(SXVL, "56")
    field(SXST, "FSn_0_417_l_min")
    field(SXVL, "57")
    field(SXST, "FSn_0_500_l_min")
    field(NOBT, "8")
    field(SHFT, "0")
}

record(longin, "$(PREFIX)$(SGNL)-ERR-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERR-RB")

    field(DESC, "$(DESC) Error Code")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+12 T=INT16" )
}

record(ai, "$(PREFIX)$(SGNL)-HIHI-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-HIHI-RB")

    field(DESC, "$(DESC) HIHI Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+14 T=REAL32" )
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL)-HIHI-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL)-HIGH-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-HIGH-RB")

    field(DESC, "$(DESC) HIGH Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+18 T=REAL32" )
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL)-HIGH-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL)-LOW-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-LOW-RB")

    field(DESC, "$(DESC) LOW Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+22 T=REAL32" )
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL)-LOW-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL)-LOLO-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-LOLO-RB")

    field(DESC, "$(DESC) LOLO Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+26 T=REAL32" )
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL)-LOLO-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL)-AOFF-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-AOFF-RB")

    field(DESC, "$(DESC) Offset RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+30 T=REAL32" )
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL)-AOFF-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL)-SVAL-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-SVAL-RB")

    field(DESC, "$(DESC) Simul Val")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+34 T=REAL32" )
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL)-SVAL-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL)") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))")

    field(DESC, "$(DESC)")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+38 T=REAL32" )
    field(MDEL, "-1")
    field(PREC, "$(PREC)")
    field(EGU, "$(EGU)")
    field(SIMS, "MINOR")
    field(HHSV, "MAJOR")
    field(HSV,  "MINOR")
    field(LSV, "MINOR")
    field(LLSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(longin, "$(PREFIX)$(SGNL)-VAL-RB") {
    $(OLD_ALIAS=#)alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-VAL-RB")

    field(DESC, "$(DESC) Raw Value")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+42 T=INT16" )
    field(MDEL, "-1")

    info(ARCHIVE_THIS, "")
}
