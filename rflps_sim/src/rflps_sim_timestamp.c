/**
 * @brief Returns formatted Timestamp string from a waveform.
 */

// Subroutine
#include <aSubRecord.h>

// Export Subroutine Function
#include <registryFunction.h>
#include <epicsExport.h>

// Normalized C Types
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static long convert_timestamp(aSubRecord *prec)
{
    char *time_values = (char *)prec->a;

    if (prec->nea < 7) {
        sprintf(prec->vala, "Error when converting timestamp");
        return 1;
    }

    sprintf(prec->vala, "%02d-%02d-%02d %02d:%02d:%02d.%d", time_values[0],time_values[1],time_values[2],time_values[3],time_values[4],time_values[5],time_values[6]);

    return 0;
}

epicsRegisterFunction(convert_timestamp);
