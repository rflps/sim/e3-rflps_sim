# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.4] - 2023-05-05

- Included new feature for Filament Black-Heat
- Upgraded S7PLC_DEP_VERSION to v1.5.2+0 in CONFIG_MODULE file

## [1.0.0] - 2022-11-14
- Added Filament DBs
- Added Info Tags for missing PVs that need to be archived
- Fixed PV names to comply with ESS Naming
- Added PV to convert Timestamp waveform retrieved from PLC to readable string
- Upgraded s7plc to v1.5.1

## [0.2.0] - 2021-11-03
- Added CANON Klsytron to the Filament operation modes

## [0.1.0] - 2021-09-15
- Addition of info tags to indicate which PVs should be archived
- Changes in the analog float read database mbbi to support more sensor types
- Fixes the formatting of database files

## [0.0.4] - 2020-07-15
- Remove colons from record names [ $(PREFIX):$(SGNL) -> $(PREFIX)$(SGNL) ]

## [0.0.3] - 2019-03-19
- Force to init the seq after reading the RB PVs from the hardware
- Changed the E3 module to a local module, without submodule

## [0.0.2] - 2019-09-25
- Changes to the PIN shutter and shutter status

## [0.0.1] - 2019-09-24
- First tagged version
- Designed as E3 wrapper + module

[0.1.0]: https://gitlab.esss.lu.se/e3/wrappers/rf/e3-rflps_sim/-/tags/7.0.5-3.4.1%2F0.1.0-76382c3-20210915T175523
[0.0.4]: https://gitlab.esss.lu.se/e3/wrappers/rf/e3-rflps_sim/-/tags/7.0.5-3.4.1%2F0.0.4-0eaa10a-20210520T120043
[0.0.3]: https://gitlab.esss.lu.se/rflps/sim/e3-rflps_plc/-/tags/v0.0.3
[0.0.2]: https://gitlab.esss.lu.se/rflps/sim/rfsimdb/-/tags/v0.0.2
[0.0.1]: https://gitlab.esss.lu.se/rflps/sim/rfsimdb/-/tags/v0.0.1
