# RFLPS Slow Interlocks Module (e3-rflps_sim)

The SIM is a local protection system based on Siemens PLC hardware. The PLC software and the EPICS IOC that implements the control system interface are designed to be generic so they can be used in all the RF systems of the ESS linac.

This module contains the common EPICS template (database) files for the RFLPS SIM PLC project and it used by any IOC instance that is part of the RFLPS system.

For a project history, see the [CHANGELOG.md](CHANGELOG.md).

### Prerequisites

All the database files of this module use the [s7plc](https://gitlab.esss.lu.se/e3/wrappers/communication/e3-s7plc) device support, so it is mandatory to have the s7plc module loaded when running any IOC that makes use of the rflps_sim module. s7plc is also define as a dependency of this project.
